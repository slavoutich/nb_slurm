# flake8: noqa
from ._version import version_info, __version__
from .kernelspecmanager import SlurmKernelSpecManager
from .kernelmanager import SlurmKernelManager

