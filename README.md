Custom KernelSpecManager for Jupyter for launching kernels remotely, using
[SLURM workload manager](https://slurm.schedmd.com/).

For each kernel found it will modify a command to launch kernel with SLURM.
Options for launching are configurable via the Jupyter config file.
