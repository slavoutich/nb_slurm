import os
import random
import re
import signal
import socket
import tempfile
from jupyter_client.connect import port_names, write_connection_file
from jupyter_client.ioloop.manager import IOLoopKernelManager
from jupyter_client.localinterfaces import is_local_ip
from pexpect import EOF, TIMEOUT
from pexpect.popen_spawn import PopenSpawn
from socket import gethostbyname
from subprocess import list2cmdline, check_output
from traitlets import Bool, Unicode, Instance, Integer, Dict, List
from .kernelspecmanager import SlurmKernelSpecManager


class SlurmAllocationError(Exception):
    pass


class SlurmKernelManager(IOLoopKernelManager):

    salloc_command = Unicode(
        default_value='salloc',
        help='salloc executable to call.',
        config=True,
        allow_none=False
    )

    srun_command = Unicode(
        default_value='srun',
        help='srun executable to call.',
        config=True,
        allow_none=False
    )

    scontrol_command = Unicode(
        default_value='scontrol',
        help='scontrol executable to call.',
        config=True,
        allow_none=False
    )

    scancel_command = Unicode(
        default_value='scancel',
        help='scancel executable to call.',
        config=True,
        allow_none=False
    )

    connection_files_dir = Unicode(
        default_value=os.path.join(tempfile.gettempdir()),
        help='Directory for storing connection files. It must be shared '
             'beyond the nodes of the cluster and users must be allowed to '
             'create directories there.',
        config=True,
        allow_none=False
    )

    shell = List(
        default_value=['sh'],
        help='Interactive shell, executed in a Jupyter kernel job (in a list '
             'of argumets format).',
        config=True,
        allow_none=False
    )

    hostname_ip_map = Dict(
        default_value={},
        help='Mapping between hostnames of SLURM nodes and IP addresses for '
             'the kernels to bind to.',
        config=True,
        allow_none=False
    )

    port_range = List(
        default_value=list(range(49152, 65536)),
        help='List of ports to select for connection to the remote kernels.',
        config=True,
        allow_none=False
    )

    slurm_timeout = Integer(
        default_value=10,
        help='Timeout in seconds, used internally while waiting for '
             'interaction with SLURM processes.',
        config=True,
        allow_none=False
    )

    default_kernel_local = Bool(
        default_value=False,
        help='Whether to default to launching a kernel locally or to launch '
             'it via SLURM with default arguments.',
        config=True,
        allow_none=False
    )

    fallback_kernel_local = Bool(
        default_value=False,
        help='Whether to fallback to launching a kernel locally, if SLURM '
             'allocation is not succeeded.',
        config=True,
        allow_none=False
    )

    default_kernel_job_name = Unicode(
        default_value='jupyter_kernel',
        help='Default name for a kernel SLURM job. Can be overridden in '
             'SlurmKernelSpecManager profile, passing argument `--job-name` '
             'to salloc.',
        config=True,
        allow_none=False
    )

    _metadata_namespace = SlurmKernelSpecManager.metadata_namespace
    _metadata_salloc_args = SlurmKernelSpecManager.metadata_salloc_args

    kernel_spec_manager = Instance(SlurmKernelSpecManager)

    def _kernel_spec_manager_default(self):
        return SlurmKernelSpecManager(data_dir=self.data_dir)

    def _ip_by_nodename(self, nodename):
        custom = self.hostname_ip_map.get(nodename)
        return custom or gethostbyname(nodename)

    def __init__(self, **kwargs):
        super(SlurmKernelManager, self).__init__(**kwargs)
        self.slurm_jobid = None
        self._ip_default = self.ip

    def start_kernel(self, **kw):
        """Starts a kernel on a remote host, using 'salloc' and 'srun' to
        allocate an interactive shell on a remote node and launching a kernel
        there with 'exec'.

        Parameters
        ----------
        `**kw` : optional
             keyword arguments that are passed down to build the kernel_cmd
             and launching the kernel. Only 'env' and 'extra_arguments' have
             an impact, but others are stored to be on the safe side.
        """
        # Prepare the environment for kernel
        env = kw.pop('env', os.environ).copy()
        # Don't allow PYTHONEXECUTABLE to be passed to kernel process.
        # If set, it can bork all the things.
        env.pop('PYTHONEXECUTABLE', None)
        if not self.kernel_cmd:
            # If kernel_cmd has been set manually, don't refer to a kernel spec
            # Environment variables from kernel spec are added to os.environ
            env.update(self.kernel_spec.env or {})

        if self.slurm_jobid:
            if self.is_alive():
                raise RuntimeError("Old SLURM kernel is not killed.")
            else:
                self.slurm_jobid = None

        # Launch a job and write a proper connection file for the kernel
        launch_local = False
        try:
            shell, nodename, jobid = self._launch_slurm_shell(env)
            if shell is None:
                launch_local = True
        except SlurmAllocationError:
            if self.fallback_kernel_local:
                shell, nodename, jobid = None, None, None
                launch_local = True
            else:
                raise RuntimeError("Could not get SLURM allocation for the"
                                   " kernel.")

        if launch_local:
            if not is_local_ip(self.ip):
                self.ip = self._ip_default
            return super(SlurmKernelManager, self).start_kernel(env=env, **kw)

        self.slurm_jobid = jobid
        self.ip = self._ip_by_nodename(nodename)
        self.write_connection_file(remote=True)

        # save kwargs for use in restart
        self._launch_args = kw.copy()
        # build the Popen cmd
        extra_arguments = kw.pop('extra_arguments', [])
        kernel_cmd = (["exec"] +
                      self.format_kernel_cmd(extra_arguments=extra_arguments))
        kernel_cmdline = list2cmdline(kernel_cmd)

        # launch the kernel subprocess
        self.log.debug("Starting kernel: %s" % kernel_cmd)
        self.log.debug("Kernel command: %s" % kernel_cmdline)
        shell.sendline(kernel_cmdline)
        self.kernel = shell
        self.start_restarter()
        self._connect_control_socket()

    def restart_kernel(self, now=False, newports=False, **kw):
        """Restarts a kernel with the arguments that were used to launch it.

        Parameters
        ----------
        now : bool, optional
            If True, the kernel is forcefully restarted *immediately*, without
            having a chance to do any cleanup action.  Otherwise the kernel is
            given 1s to clean up before a forceful restart is issued.

            In all cases the kernel is restarted, the only difference is whether
            it is given a chance to perform a clean shutdown or not.

        newports : ingored, always True effectively
            Left for the compatibility with Jupyter's reference
            implementation. We can't guarantee, that node of the new kernel
            will be the same, so ports are always cleaned and assigned freshly.

        `**kw` : optional
            Any options specified here will overwrite those used to launch the
            kernel.
        """
        if self._launch_args is None:
            raise RuntimeError("Cannot restart the kernel. "
                               "No previous call to 'start_kernel'.")
        else:
            # Stop currently running kernel. Completely.
            self.shutdown_kernel(now=now, restart=False)
            self.cleanup_random_ports()

            # Start new kernel.
            self._launch_args.update(kw)
            self.start_kernel(**self._launch_args)

    def _launch_slurm_shell(self, env):
        """Launches an interactive SLURM shell with 'salloc' and 'srun' and
        returns information about it.

        Parameters
        ----------
        `env` : dict or None
            A dictionary of environment variables, passed to the process. If
            None, shell inherits parent process' environment.
        """
        metadata = self.kernel_spec.metadata.get(self._metadata_namespace, None)
        if metadata is None:
            raise SlurmAllocationError("SLURM metadata for the kernel"
                                       " not found.")

        salloc_args = metadata.get(self._metadata_salloc_args, None)
        self.log.debug("salloc_args: %s" % salloc_args)
        if salloc_args is None:
            if self.default_kernel_local:
                return None, None, None
            else:
                salloc_args = []

        # allocating a SLURM job with a shell and getting allocated node
        # noinspection PyTypeChecker
        cmd = ([self.salloc_command,
                # Ensure that we have at least one CPU, can be overridden
                '--cpus-per-task=1',
                # Set job name, can be overridden
                '--job-name=%s' % self.default_kernel_job_name] +
               # output information with a node name in salloc output
               salloc_args +
               # Ensure that we have only one node allocated
               ['--nodes=1',
                # Double verbosity even if user tries to do otherwise
                '-vv'] +
               # srun
               [self.srun_command, '--pty'] + self.shell)

        # Getting interactive shell and node, where it is running
        job = None
        try:
            job = PopenSpawn(cmd, env=env, timeout=self.slurm_timeout)
            # Determine SLURM jobid
            job.expect("salloc:.*Granted job allocation (.*)")
            jobid = job.match.groups()[0]
            # Determine allocated host
            job.expect(
                "salloc:.*laying out the 1 tasks on 1 hosts (.*) dist .*",
                timeout=self.slurm_timeout)
            # This uses to be the last line of the salloc output
            # We can send the kernel launch command afterwards
            node = job.match.groups()[0]
            return job, str(node.decode()), int(jobid.strip())
        except EOF:
            if job:
                job.kill(1)
            raise SlurmAllocationError(
                "`salloc` has exited without opening a shell.\nPlease check"
                " whether you have provided correct parameters in "
                "SlurmKernelSpecManager configuration.")
        except TIMEOUT:
            if job:
                job.kill(1)
            raise SlurmAllocationError(
                "Timeout while waiting for allocation of a job via `salloc`.\n"
                "May be target partition is full?"
            )

    def write_connection_file(self, remote=False):
        """Write connection info to JSON dict and assign
        self.connection_file.

        Parameters
        ----------
        `remote` : boolean
            If False, parent's method is called (local kernel and ports are
            assigned via `socket.bind`).
            If True, kernel is treated as remote one, and custom procedures
            are used to generate a valid connection file.
        """
        # Default is parent's realization, otherwise it will break calling
        # parent's methods.
        if not remote:
            return super(SlurmKernelManager, self).write_connection_file()

        if (self._connection_file_written and
                os.path.exists(self.connection_file)):
            return

        # Connection dir is set on the configuration level, also per-user subdir
        # is needed
        connection_dir = os.path.join(self.connection_files_dir,
                                      str(os.geteuid()))
        if not os.path.exists(connection_dir):
            os.makedirs(connection_dir)
        fd, fname = tempfile.mkstemp('.json', dir=connection_dir)
        os.close(fd)
        # We manage ports ourselves, `write_connection_file`'s routine will fail
        self._record_random_port_names()
        self._assign_port_numbers()
        # Actually, write connection file
        self.connection_file, _ = write_connection_file(
            fname, transport=self.transport, ip=self.ip, key=self.session.key,
            stdin_port=self.stdin_port, iopub_port=self.iopub_port,
            shell_port=self.shell_port, hb_port=self.hb_port,
            control_port=self.control_port,
            signature_scheme=self.session.signature_scheme,
            kernel_name=self.kernel_name
        )

        self._connection_file_written = True

    def _assign_port_numbers(self):
        """Assign random port numbers on a remote machine to bind to.

        Canonical realization of KernelManager relies on assigning random
        port numbers by the system, passing all the ports as 0 by default.
        Since we are launching a kernel on a remote machine, we cannot rely
        on this. However, we can at least try to connect to these ports to
        see, whether they are available or not.
        """
        # noinspection PyTypeChecker
        ports = tuple(random.sample(self.port_range, len(self.port_range)))
        ports_iter = iter(ports)
        self._record_random_port_names()
        for name in port_names:
            setattr(self, name,
                    self._assign_port(ports_iter, port=getattr(self, name, 0)))

    def _assign_port(self, ports_iter, port=0):
        """If `port` is specified already (>0), its value is returned.
        Tests if port is not available on a machine with IP `self.ip` and,
        if it is not, returns port number. If no more ports left to test,
        raises a RuntimeError
        """
        if port > 0:
            return port
        try:
            while True:
                port = next(ports_iter)
                if self._test_port_not_available(port):
                    return port
        except StopIteration:
            raise RuntimeError("Cannot assign a port, all ports on a remote "
                               "machine are occupied.")

    def _test_port_not_available(self, port):
        """Tests if we can open a TCP connection to the port and returns
        True, if we can't.
        """
        sock = socket.socket()
        try:
            sock.connect((self.ip, port))
            return False
        except ConnectionRefusedError:
            return True

    def _kill_kernel(self):
        """Kill the running kernel."""
        if self.has_kernel:
            if self.slurm_jobid:
                self.kernel.kill(1)
                self.kernel.wait()
                self.kernel = None
                self.slurm_jobid = None
            else:
                # Kernel is local, calling parent's method
                super(SlurmKernelManager, self)._kill_kernel()
        else:
            raise RuntimeError("Cannot kill kernel. No kernel is running!")

    def signal_kernel(self, signum):
        """Sends a signal to the shell, where the kernel is being executed."""
        if self.has_kernel:
            if self.slurm_jobid:
                try:
                    sig = re.match("SIG(\w+)", signum.name).groups()[0]
                except AttributeError:
                    sig = str(signal)
                # Signaling kernel process group using scancel
                check_output([self.scancel_command,
                              '--signal=%s' % sig,
                              str(self.slurm_jobid)])
            else:
                # Kernel is local, calling parent's method
                super(SlurmKernelManager, self).signal_kernel(signum=signum)
        else:
            raise RuntimeError("Cannot signal kernel. No kernel is running!")

    def is_alive(self):
        """Kernel is considered as alive, if its job exists and its state is
        RUNNING, or it is launched locally and parent tells so.
        """
        if self.slurm_jobid:
            out = PopenSpawn([self.scontrol_command,
                              "show", "job", str(self.slurm_jobid)],
                             timeout=self.slurm_timeout)
            try:
                out.expect("JobState=(\w+)")
                state = out.match.groups()[0]
                if str(state.decode()) == 'RUNNING':
                    return True
                else:
                    return False
            except EOF:
                return False
        else:
            return super(SlurmKernelManager, self).is_alive()

    # Backwards compatibility
    # At the moment of writing functions below are not landed to the stable
    # jupyter_client (5.1.0), so we will put them here as a workaround
    # TODO: remove when landed and require appropriate jupyter_client version

    if not hasattr(IOLoopKernelManager, "_random_port_names"):
        # names of the ports with random assignment
        _random_port_names = None

    if not hasattr(IOLoopKernelManager, "_record_random_port_names"):
        def _record_random_port_names(self):
            """Records which of the ports are randomly assigned.

            Records on first invocation, if the transport is tcp.
            Does nothing on later invocations."""

            if self.transport != 'tcp':
                return
            if self._random_port_names is not None:
                return

            self._random_port_names = []
            for name in port_names:
                if getattr(self, name) <= 0:
                    self._random_port_names.append(name)

    if not hasattr(IOLoopKernelManager, "cleanup_random_ports"):
        def cleanup_random_ports(self):
            """Forgets randomly assigned port numbers and cleans up the connection file.

            Does nothing if no port numbers have been randomly assigned.
            In particular, does nothing unless the transport is tcp.
            """

            if not self._random_port_names:
                return

            for name in self._random_port_names:
                setattr(self, name, 0)

            self.cleanup_connection_file()
