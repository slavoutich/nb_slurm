################################################################################
### Options, needed to set to enable nb_slurm                                ###
################################################################################

## The kernel spec manager class to use. Should be a subclass of
#  `jupyter_client.kernelspec.KernelSpecManager`.
#
#  The Api of KernelSpecManager is provisional and might change without warning
#  between this version of Jupyter and the next stable one.
c.NotebookApp.kernel_spec_manager_class = 'nb_slurm.SlurmKernelSpecManager'

## The kernel manager class.  This is configurable to allow subclassing of the
#  KernelManager for customized behavior.
c.MultiKernelManager.kernel_manager_class = 'nb_slurm.SlurmKernelManager'

## Directory for storing connection files. It must be shared beyond the nodes
#  of the cluster and users must be allowed to create directories there.
c.SlurmKernelManager.connection_files_dir = '/path/to/shared/folder/.connection_files'


################################################################################
### Other configuration options                                              ###
################################################################################

# Dictionary of profiles.
# Keys are names, that are appended to the original kernel name. They should be
# alphanumeric (no spaces).
# Values are list of additional arguments, that are passed to the `salloc`
# command. See `man salloc` for more information.
# Default is:

#c.SlurmKernelSpecManager.profiles = {'default': []}

# More advanced example:

#c.SlurmKernelSpecManager.profiles = {'debug': ['--partition=debug',
#                                               '--account=user'],
#                                     'comp': ['--partition=computation',
#                                              '--account=user',
#                                             '--cpus-per-task=4']}

# salloc executable to call.
#c.SlurmKernelManager.salloc_command = 'salloc'

## srun executable to call.
#c.SlurmKernelManager.srun_command='srun',

## scontrol executable to call.
#c.SlurmKernelManager.scontrol_command = 'scontrol',

## scancel executable to call.
#c.SlurmKernelManager.scancel_command = 'scancel'

## Interactive shell, executed in a Jupyter kernel job
#  (in a list of argumets format).
#c.SlurmKernelManager.shell = ['sh']

## Mapping between hostnames of SLURM nodes and IP addresses for the kernels
#  to bind to.
#c.SlurmKernelManager.hostname_ip_map = {},
# More advanced example:
#c.SlurmKernelManager.hostname_ip_map = {'mynodename1': '10.0.0.1',
#                                        'mynodename1.example.com': '10.0.0.1'
#                                        'mynodename10': '10.0.0.10'}

## List of ports to select for connection to the remote kernels.',
#c.SlurmKernelManager.port_range = list(range(49152, 65536)),

## Timeout in seconds, used internally while waiting for interaction with
# SLURM processes.
#c.SlurmKernelManager.slurm_timeout = 10

## Whether to default to launching a kernel locally or to launch it via SLURM
#  with default arguments.
#c.SlurmKernelManager.default_kernel_local = False

## Whether to fallback to launching a kernel locally, if SLURM allocation is
#  not succeeded.',
#c.SlurmKernelManager.fallback_kernel_local = False

## Default name for a kernel SLURM job. Can be overridden in
#  SlurmKernelSpecManager profile, passing argument `--job-name` to salloc.
#c.SlurmKernelManager.default_kernel_job_name = 'jupyter_kernel',

