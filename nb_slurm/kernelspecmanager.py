from traitlets import Dict
from itertools import product
from jupyter_client.kernelspec import KernelSpec, NoSuchKernel

# Defaulting to CondaKernelSpecManager, if found
try:
    from nb_conda_kernels import CondaKernelSpecManager as KernelSpecManager
except ImportError:
    from jupyter_client.kernelspec import KernelSpecManager


# noinspection PyClassHasNoInit
class SlurmKernelSpecManager(KernelSpecManager):
    """For each Jupyter kernel profile its parent finds this class generates
    a set of child profiles, that are used to execute these kernels with
    different SLURM parameters. Parameters are configurable via Jupyter
    config and passed to the Kernel Manager via KernelSpec metadata.
     """

    profiles = Dict(
        default_value={'default': None},
        help=(
            'Dictionary of profiles.'
            'Keys are names, that are appended to the original kernel name.\n'
            'They should be alphanumeric (no spaces).\n'
            'Values are list of additional arguments, that are passed '
            'to the `salloc` command, or None.\n'
            'None invokes default behaviour of `SlurmKernelManager`.\n'
            'See `man salloc` for more information.'
        ),
        config=True,
        allow_none=False
    )

    _visual_separator = ' | '
    _kn_separator = '___slurm___'

    metadata_namespace = 'nb_slurm'
    metadata_salloc_args = 'salloc_args'

    def find_kernel_specs(self):
        """Gets kernel spec from `nb_conda_kernels.CondaKernelSpecManager` or
        `jupyter_client.kernelspec.KernelSpecManager` and generates SLURM
        profiles for each of them.
        """
        kspecs = super(SlurmKernelSpecManager, self).find_kernel_specs()
        return {
            self._kn_separator.join((kernel_name, profile_name)): profile_loc
            for (kernel_name, profile_loc), profile_name in product(
                kspecs.items(), self.profiles.keys())
        }

    def get_kernel_spec(self, kernel_name):
        """Returns a :class:`KernelSpec` instance for the given kernel_name.

        Raises KeyError if the given kernel name is not found.
        """
        splitted = kernel_name.split(self._kn_separator)
        orig_name = self._kn_separator.join(splitted[:-1])
        profile_name = splitted[-1]
        # Here we need to get the original KernelSpec, but reference
        # implementation of KernelSpec calls `self.find_kernel_specs()`, which
        # is overridden in this class, so we have to carefully realize the
        # same logic to avoid getting kernels with modified names
        try:
            # Conda environment
            kspec_orig = super(SlurmKernelSpecManager, self) \
                .get_kernel_spec(orig_name)
        except NoSuchKernel:
            # jupyter native environment
            # Above will fail, because we have overridden find_kernel_specs
            # We need to repeat `jupyter_client.kernelspec.KernelSpecManager`
            # logic explicitly
            d = super(SlurmKernelSpecManager, self).find_kernel_specs()
            try:
                resource_dir = d[orig_name.lower()]
            except KeyError:
                raise NoSuchKernel(kernel_name)
            kspec_orig = self._get_kernel_spec_by_name(orig_name, resource_dir)

        # noinspection PyUnresolvedReferences
        metadata = {self.metadata_namespace:
                    {self.metadata_salloc_args: self.profiles[profile_name]}}

        # Need to copy before modifying, since CondaKernelSpecManager uses
        # caching
        try:
            kspec = KernelSpec(resource_dir=kspec_orig.resource_dir,
                               **kspec_orig.to_dict())
        except AttributeError:
            kspec = KernelSpec(**kspec_orig.to_dict())
        kspec.display_name = (kspec_orig.display_name +
                              self._visual_separator + profile_name)
        try:
            kspec.metadata.update(metadata)
        except AttributeError:
            kspec.metadata = metadata

        return kspec
