#!/usr/bin/env python
# coding: utf-8

# Copyright (c) Viacheslav Ostroukh
# Distributed under terms of 2-clause BSD license

from os.path import exists, join
from setuptools import setup


with open(join('nb_slurm', '_version.py')) as version:
    exec(version.read())

install_requires = [
    'jupyter_client',
    'traitlets',
    'pexpect',
    'six'
]

setup(name='nb_slurm',
      version=__version__,
      description='Kernel manager for Jupyter for launching kernels via SLURM',
      url='https://gitlab.com/slavoutich/nb_slurm',
      maintainer='Viacheslav Ostroukh',
      maintainer_email='v.dev@ostroukh.me',
      license='BSD 2-clause',
      keywords='',
      packages=['nb_slurm'],
      install_requires=install_requires,
      long_description=(open('README.md').read() if exists('README.md')
                        else ''),
      zip_safe=False)
